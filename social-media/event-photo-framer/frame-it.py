from PIL import Image, ImageOps
import os


imgs_folder = os.path.dirname(os.path.realpath(__file__)) + '/pics'
output_folder = os.path.dirname(os.path.realpath(__file__)) + '/output'

foreground = Image.open('frame.svg.png')
foreground_size = (foreground.width, foreground.height)

for filename in os.listdir(imgs_folder):
    if filename.endswith(".jpg") or filename.endswith(".JPG"):
        img_path = os.path.join(imgs_folder, filename)
        background = Image.open(img_path)

        container = ImageOps.fit(background, foreground_size, Image.ANTIALIAS)

        container.paste(foreground, (0, 0), foreground)
        container.save(os.path.join(output_folder, filename), "JPEG")
    else:
        continue
