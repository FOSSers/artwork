'''
Copyleft Subin Siby <subins2000@gmail.com>
Licensed under GPL-3.0
'''

from urllib.request import urlopen

import csv
import codecs
import getopt
import os
import sys
import yaml


def csvToYAML(csvFile):
    csvFile = open(csvFile, 'rb')
    stream = open('temp.yaml', 'w', encoding='utf-8')

    csvOpen = csv.reader(codecs.iterdecode(csvFile, 'utf-8'))
    keys = next(csvOpen)

    output = {
        'replace_info': []
    }

    for row in csvOpen:
        # Exit if fields are empty
        if row[0] == 'volunteers':
            return

        output['replace_info'].append(dict(zip(keys, row)))

    yaml.dump(
        [output],
        stream,
        default_flow_style=False,
    )

    stream = open('temp.yaml', 'r', encoding='utf-8')
    data = stream.read()

    output = open('certificates.yaml.template', 'r', encoding='utf-8')
    # To remove leading '- ', use 2:
    compiledTemplate = output.read().replace('{{data}}', data[2:])

    stream = open('certificates.yaml', 'w', encoding='utf-8')
    stream.write(compiledTemplate)
    stream.close()

    os.remove('temp.yaml')


csvToYAML('certificates.csv')
